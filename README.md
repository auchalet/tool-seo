**Free Tool SEO**
=============

Développement d'un outil d'aide au référencement
------------------------------------------------
Utilisation du micro-Framework Silex
[Installation & Documentation](http://silex.sensiolabs.org/documentation)

----------

Idées de fonctionnalités
-------------

 - Outil de scraping
 - Scrapping de listes de proxy
 - Outil de suivi des positions
 - Comparateur de plusieurs pages
 - Calcul du TF-IDF d'un mot clé pour un corpus
 - Calcul de la densité d'un mot clé dans une page ou sur un texte brut
 - Création d'un sitemap + pouvoir ajouter des listes d'URL dynamiquement
 - Content Spinner (contenu aléatoire avec synonymes)

Licence
-------------
Libre et Open Source. Nom de la licence à déterminer (GNU/GPL 3 ?)
