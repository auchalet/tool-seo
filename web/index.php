<?php

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Silex\Application();

$app->get('/', function() {
    return 'Index';
});


$app->get('/didou/{name}', function($name) use ($app) {
    return 'Hello '.$app->escape($name);
});



$app->run();
